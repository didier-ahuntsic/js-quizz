export interface QuestionSource {
    toString(): string;
}

export type QuestionMeta =  {
    question_fr: string,
    validate: (answer: string) => boolean,
    answer: string | number,
    id: string,
}