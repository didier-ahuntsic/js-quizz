import { sha1 } from "https://denopkg.com/chiefbiiko/sha1/mod.ts";


function basicFrenchQuestion(argumentValue){
    return `Quel est la valeur retournée par la fonction si la valeur de l'argument x est ${argumentValue}?`
}

function basicQuestionValidator(expected){
    const cannonicalString = (v) => {
        if(v === null){
            return 'null';
        }
        if(v === undefined){
            return 'undefined'
        }
        return v.toString()

    }

    return (answer) => {
        return cannonicalString(answer) === cannonicalString(expected);
    }

}

function basicQuestionMetaData(question){
    const sourceHash = sha1(question.toString(), "utf8", "hex");
    return (x, idx) => ({
        sourceHash: sourceHash,
        idx: idx,
        id: `${sourceHash}-${idx}`,
        question_fr: basicFrenchQuestion(x),
        validate: basicQuestionValidator(question(x)),
        answer: question(x),
    });
}

export function question001(x){
    if(x === 0){
        x = x + 1;
    }
    return x;
}


question001.questions = [0, 16].map(basicQuestionMetaData(question001));

export function question002(x){
    if(x === 0){
        x = x + 1;
    } else {
        x = x - 1
    }
    return x;
}

question002.questions = [0, 6].map(basicQuestionMetaData(question002));


export function question003(x){
    if(x === 0){
        x = x + 1;
    } else {
        x = x - 1
    }
    return x;
}

question003.questions = [0, 17].map(basicQuestionMetaData(question003));

export function question004(x){
    if(x === 0){
        x = 3;
    } else if (x === 3){
      x = x + 1;
    }else {
        x = x - 1
    }
    return x;
}

question004.questions = [0, 3, 8].map(basicQuestionMetaData(question004));


export function question005(x){
    for(let i = 0; i < 10; i ++ ){
        if (i == x){
            return 0;
        }
    }
    return x;
}

question005.questions = [0, 3, 8, -2, 10].map(basicQuestionMetaData(question005));


export function question006(x){
    if(x < 10){
        return 54;
    }
    if(x < 20){
        return 23;
    }
    if(x < 30){
        return 232;
    }
    return 15;
}

question006.questions = [-1, 3, 10, 20, 30, 26, 31, 12].map(basicQuestionMetaData(question006));

export function question007(x){
    for(let i = 0; i < 10; i += 2){
        if(i === x){
            return 0
        }
    }
    return 1;
}

question007.questions = [-1, 2, 7, 12, 13].map(basicQuestionMetaData(question007))



export function question008(x){
    for(let i = 0; i < 10; i += 2){
        if(i === x){
            return 0
        }
    }
    return 1;
}

question008.questions = [-1, 2, 7, 12, 13].map(basicQuestionMetaData(question008))


export function question009(x){
    let y = 10;
    if(x < 10){
        x = x + y;
    }else if(x < 100){
        y -= 90
    } else {
        x = x * 2;
    }
    if(y > 0){
        return x;
    } else {
        return y;
    }
}

question009.questions = [2, 10, 23, 100,].map(basicQuestionMetaData(question009))


export function question010(x){
    if((x < 10 && x > 0) || (x < -10 && x > -20)) {
        return 4;
    }
    return 3;
}

question010.questions = [5, -2, -13, 34].map(basicQuestionMetaData(question010))


export function question011(x){
    if(x < 10 && x > 0) {
        return 7;
    }

    if(x < -10 && x > -20){
        return 2;
    }
    return 1;
}

question011.questions = [5, -2, -13, 34].map(basicQuestionMetaData(question011))



export function question012(x){
    let y = 15
    if(x < 10 && x > 0) {
        y = 7;
    } else if(x < -10 && x > -20){
        y = 2;
    }
    return y;
}

question012.questions = [5, -2, -13, 34].map(basicQuestionMetaData(question012))


export function question013(x){
    const ret = []
    for(let i = 0; i < 10; i += x){
        ret.push(i)
    }
    return ret[0]
}

question013.questions = [1, 3, 5].map(basicQuestionMetaData(question013))


export function question014(x){
    const ret = []
    for(let i = 0; i < 10; i += x){
        ret.push(i)
    }
    return ret[ret.length - 1]
}

question014.questions = [1, 3, 5].map(basicQuestionMetaData(question014))




export function question015(x){
    if(x <= 0){
        return 0;
    }
    return question015(x - 1);
}

question015.questions = [-4, 2, 7].map(basicQuestionMetaData(question015))


export function question016(id){
    const people = [{id: 12, name: "bob"}, 
                    {id: 34, name: "alice"}, 
                    {id: 56, name: "cedric"},
                    {id: 6, name: "david"}, ]
    for(const person of people){
        if (person.id === id){
            return person;
        }
    }
}

question016.questions = [12, 56, 7].map(basicQuestionMetaData(question016))


