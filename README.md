# JS quizz

Small duolingo like web application designed to practice javascript code reading.  The application
is deployed at https://js.all-dressed-programming.com/ .


## Development

### Technologies

#### Backend
The backend is made in typescript using the deno runtime.  The web framework is oak.
The database used is denoKV.   Currently, the database is only used to store
usage statistics.

#### Frontend
The frontend is using htmx.

#### Deployment
The deployment is done on deno deploy.
